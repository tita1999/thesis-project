import Load_Preprocessing
import pandas as pd
df = pd.read_csv("Users_Cats.csv")

def new_pred(vect):
    n_samples,n_clusters = vect.shape[0],centroids.shape[0]

    distances = np.zeros((n_samples,n_clusters))
    for i in range(n_clusters):
        centroid = np.tile(centroids[i,:],(n_samples,1))
        den1 = 1-np.linalg.norm(vect)**2
        den2 = 1-np.linalg.norm(centroid, axis=1)**2
        the_num = np.linalg.norm(vect-centroid, axis=1)**2
        distances[:,i] = np.arccosh(1+2*the_num/(den1*den2))
    return np.argmin(distances)

def weight_average(all_cats):
    clust = []
    for x in all_cats:
        vect = model.get_vector(x)
        new_clust = new_pred(vect)
        clust.append(new_clust)

    a = np.unique(clust, return_counts=True)
    weighted_avg = np.average(a[0], weights=a[1])
    return weighted_avg



#Get new insights - implementation of false likes

def get_insights(model, pos_cats, neg_cats):
    
    Insights = []
    final_insights = []
    final = []

    #Get the new insights by getting the most similar categories
    embedding = model
    for y in pos_cats:
        insights = embedding.most_similar(y)[:2]
        insights = [x[0] for x in insights]
        Insights.append(insights)

    insights = [item for sublist in Insights for item in sublist]

    #We need to take off the insights that are related with negative categories
    for x in neg_cats:
        if x in insights:
            insights.remove(x)
            
        remove = embedding.most_similar(x)[:1]
        print(remove)
        if remove in insights:
            insights.remove(remove)

    Insights_final = list(dict.fromkeys(insights))
            
    Insights_final.sort()
    pos_cats.values.sort()

    final.append([i for i, j in zip(Insights_final, pos_cats) if i != j])
                
        
    return final

def new_user_insights(data_path):
    
    final_insights = []
    users_group = []
    categories = []
    full_insights = []

    print("Start with preprocessing...")
    all_cats = Load_Preprocessing.users_profile_data(data_path, 1)
    try:
        neg_cats = Load_Preprocessing.users_non_profile_data(data_path)
    except:
        neg_cats = []

    print("Preprocessing done.")

    model = Load_Preprocessing.load_poincare_gensim("output_file_200D_20N_0B_0R_200Ep.tsv")

    final = get_insights(model, all_cats, neg_cats)
    print(final)
    print("Finding new insights of the user.")
    for x in final:
        for y in x:
            final_insights.append(y)

    for x in final_insights:
        for y in df.columns:
            if x in y[-len(x):]:
                x = x.replace(x,y)
                full_insights.append(x)
    print("Done")
    return full_insights