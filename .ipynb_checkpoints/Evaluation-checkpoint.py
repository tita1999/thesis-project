
import pandas as pd
import numpy as np
import string
import os

#graph
from Visualization import poincare_2d_visualization


#gensim
from gensim.models.poincare import PoincareModel, PoincareKeyedVectors, ReconstructionEvaluation, LinkPredictionEvaluation
from gensim.test.utils import datapath
from gensim.utils import check_output
from collections import Counter, defaultdict
import itertools
import logging 
from gensim import utils, matutils
import csv

class ReconstructionEvaluation:
    """Evaluate reconstruction on given network for given embedding."""

    def __init__(self, file, embedding):
        
        """Initialize evaluation instance with tsv file containing relation pairs and embedding to be evaluated.

        Parameters
        ----------
        file_path : str
            Path to tsv file containing relation pairs.
        embedding : :class:`~gensim.models.poincare.PoincareKeyedVectors`
            Embedding to be evaluated.

        """
        items = set()
        relations = defaultdict(set)
        #with utils.open(file_path, 'r') as f:
            #reader = csv.reader(f, delimiter='\t')
        for row in file:
            assert len(row) == 2, 'Hypernym pair has more than two items'
            item_1_index = embedding.get_index(row[0])
            item_2_index = embedding.get_index(row[1])
            relations[item_1_index].add(item_2_index)
            items.update([item_1_index, item_2_index])
        self.items = items
        self.relations = relations
        self.embedding = embedding

    @staticmethod
    def get_positive_relation_ranks_and_avg_prec(all_distances, positive_relations):
        """Compute ranks and Average Precision of positive relations.

        Parameters
        ----------
        all_distances : numpy.array of float
            Array of all distances (floats) for a specific item.
        positive_relations : list
            List of indices of positive relations for the item.

        Returns
        -------
        (list of int, float)
            The list contains ranks of positive relations in the same order as `positive_relations`.
            The float is the Average Precision of the ranking, e.g. ([1, 2, 3, 20], 0.610).

        """
        positive_relation_distances = all_distances[positive_relations]
        negative_relation_distances = np.ma.array(all_distances, mask=False)
        negative_relation_distances.mask[positive_relations] = True
        # Compute how many negative relation distances are less than each positive relation distance, plus 1 for rank
        ranks = (negative_relation_distances < positive_relation_distances[:, np.newaxis]).sum(axis=1) + 1
        map_ranks = np.sort(ranks) + np.arange(len(ranks))
        avg_precision = ((np.arange(1, len(map_ranks) + 1) / np.sort(map_ranks)).mean())
        return list(ranks), avg_precision

    def evaluate(self, max_n=None):
        """Evaluate all defined metrics for the reconstruction task.

        Parameters
        ----------
        max_n : int, optional
            Maximum number of positive relations to evaluate, all if `max_n` is None.

        Returns
        -------
        dict of (str, float)
            (metric_name, metric_value) pairs, e.g. {'mean_rank': 50.3, 'MAP': 0.31}.

        """
        mean_rank, map_ = self.evaluate_mean_rank_and_map(max_n)
        return {'mean_rank': mean_rank, 'MAP': map_}

    def evaluate_mean_rank_and_map(self, max_n=None):
        """Evaluate mean rank and MAP for reconstruction.

        Parameters
        ----------
        max_n : int, optional
            Maximum number of positive relations to evaluate, all if `max_n` is None.

        Returns
        -------
        (float, float)
            (mean_rank, MAP), e.g (50.3, 0.31).

        """
        ranks = []
        avg_precision_scores = []
        for i, item in enumerate(self.items, start=1):
            if item not in self.relations:
                continue
            item_relations = list(self.relations[item])
            item_term = self.embedding.index_to_key[item]
            item_distances = self.embedding.distances(item_term)
            positive_relation_ranks, avg_precision = \
                self.get_positive_relation_ranks_and_avg_prec(item_distances, item_relations)
            ranks += positive_relation_ranks
            avg_precision_scores.append(avg_precision)
            if max_n is not None and i > max_n:
                break
        return np.mean(ranks), np.mean(avg_precision_scores)
