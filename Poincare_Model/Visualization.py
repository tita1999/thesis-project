
#classic
import pandas as pd
import numpy as np
import string
import os
import plotly.graph_objs as go

#gensim
from gensim.models.poincare import PoincareModel, PoincareKeyedVectors, LinkPredictionEvaluation
from gensim.test.utils import datapath
from gensim.utils import check_output
from collections import Counter, defaultdict
import itertools
import logging 
from gensim import utils, matutils
import csv


def poincare_2d_visualization(model, tree, figure_title, num_nodes=50, show_node_labels=()):
    vectors = model.vectors
    if vectors.shape[1] != 2:
        raise ValueError('Can only plot 2-D vectors')
    node_labels = model.index_to_key
    nodes_x = list(vectors[:, 0])
    nodes_y = list(vectors[:, 1])
    nodes = go.Scatter(
        x=nodes_x, y=nodes_y,
        mode='markers',
        marker=dict(color='rgb(30, 100, 200)'),
        text=node_labels,
        textposition='bottom center'
    )
    nodes_x, nodes_y, node_labels = [], [], []
    for node in show_node_labels:
        vector = model[node]
        nodes_x.append(vector[0])
        nodes_y.append(vector[1])
        node_labels.append(node)
    nodes_with_labels = go.Scatter(
        x=nodes_x, y=nodes_y,
        mode='markers+text',
        marker=dict(color='rgb(200, 100, 200)'),
        text=node_labels,
        textposition='bottom center'
    )
    node_out_degrees = Counter(hypernym_pair[1] for hypernym_pair in tree)
    if num_nodes is None:
        chosen_nodes = list(node_out_degrees.keys())
    else:
        chosen_nodes = list(sorted(node_out_degrees.keys(), key=lambda k: -node_out_degrees[k]))[:num_nodes]
    edges_x = []
    edges_y = []
    for u, v in tree:
        if not(u in chosen_nodes or v in chosen_nodes):
            continue
        vector_u = model[u]
        vector_v = model[v]
        edges_x += [vector_u[0], vector_v[0], None]
        edges_y += [vector_u[1], vector_v[1], None]
    edges = go.Scatter(
        x=edges_x, y=edges_y, mode="lines", hoverinfo='none',
        line=dict(color='rgb(50,50,50)', width=1))
    layout = go.Layout(
        title=figure_title, showlegend=False, hovermode='closest', width=800, height=800)
    return go.Figure(data=[edges, nodes, nodes_with_labels], layout=layout)
