from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import pickle as pkl
import random as rd
import tensorflow as tf


from curvlearn.manifolds import PoincareBall
from curvlearn.optimizers import RAdam
from RecommenderSystem import config

pos_set = None


training_config = {
    "manifold"             : PoincareBall(),  # The manifold to be used in the model
    "init_curvature"       : -1.0,  # Default value of the curvature of the manifold
    "trainable_curvature"  : True,  # Whether the curvatures are trainable

    "training_steps"       : 10000,  # Steps for training
    "log_steps"            : 100,  # Log intervals during training
    "eval_steps"           : 1000,  # Evaluation intervals during training

    "batch_size"           : 700,  # Batch size used in training
    "learning_rate"        : 0.01,  # Learning rate of training
    "optimizer"            : RAdam,  # The optimizer to be used in training

    "dim"                  : 200, # Dimension of the embedding tables
    "margin"               : 1.0,
    "candidate_num"        : 200,  # Sampled negative candidates for evaluation
    "K"                    : 10,  # Metric of HR@K

    "gamma"                : 0.1,  # Multi-task learning rate between the two types of loss
    "epsilon"              : 1e-9,  # Small value for avoiding dividing zeros in distortion optimization (loss)

    "embedding_initializer": tf.initializers.glorot_uniform(),  # Initializer for the embedding tables
    "shuffle_buffer"       : 10000,  # Buffer size for TF's shuffling over the dataset
}

def load_data(data):
    global user_count, item_count, pos_set
    user_count = len(data)+1
    item_count = max([max(_) for _ in data]) + 1
    train_data, test_data = [], []
    for uid, item_list in enumerate(data):
        for i in item_list[1:]:
            train_data.append((uid + 1, i))
        test_data.append((uid + 1, item_list[1]))
    record_count = len(train_data)
    print("[TRAINING] Loading data completed! User count: {}, item count: {}, training record count: {}".format(
        user_count, item_count, record_count))
    pos_set = set(train_data)
    train_data = np.array(train_data)
    return train_data, np.array(test_data), user_count, item_count

def construct_batch(ui_pair):
    while True:
        neg_i = rd.randint(1, item_count - 1)
        if (ui_pair[0], neg_i) not in pos_set:
            break
    return np.array((list(ui_pair) + [neg_i]), dtype=np.int32)


def construct_test(ui_pair):
    candidate_set = {ui_pair[1]}
    for _ in range(training_config["candidate_num"]):
        while True:
            neg_i = rd.randint(1, item_count - 1)
            if neg_i not in candidate_set and (ui_pair[0], neg_i) not in pos_set:
                candidate_set.add(neg_i)
                break
    candidate_set.remove(ui_pair[1])
    return np.array(list(ui_pair) + list(candidate_set), dtype=np.int32)


def get_train_batch(data):
    dataset = tf.data.Dataset.from_tensor_slices(data)
    dataset = dataset.map(lambda x: tf.py_func(construct_batch, [x], tf.int32))
    dataset = dataset.shuffle(buffer_size=training_config["shuffle_buffer"]).batch(training_config["batch_size"],
                                                                                   drop_remainder=False).repeat()
    data_iter = dataset.make_one_shot_iterator()
    batch = data_iter.get_next()
    return batch

def get_test_batch(data):
    dataset = tf.data.Dataset.from_tensor_slices(data)
    dataset = dataset.map(lambda x: tf.py_func(construct_test, [x], tf.int32))
    dataset = dataset.batch(training_config["batch_size"])
    data_iter = dataset.make_initializable_iterator()
    init = data_iter.initializer
    batch = data_iter.get_next()
    return batch, init
