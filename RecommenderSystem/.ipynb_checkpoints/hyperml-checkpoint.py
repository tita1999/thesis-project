from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
from curvlearn.manifolds import PoincareBall
from curvlearn.optimizers import RAdam

import numpy as np
import pickle as pkl
import random as rd

training_config = {
    "manifold"             : PoincareBall(),  # The manifold to be used in the model
    "init_curvature"       : -2.0,  # Default value of the curvature of the manifold
    "trainable_curvature"  : True,  # Whether the curvatures are trainable

    "training_steps"       : 30000,  # Steps for training
    "log_steps"            : 100,  # Log intervals during training
    "eval_steps"           : 1000,  # Evaluation intervals during training

    "batch_size"           : 700,  # Batch size used in training
    "learning_rate"        : 0.005,  # Learning rate of training
    "optimizer"            : RAdam,  # The optimizer to be used in training

    "dim"                  : 200, # Dimension of the embedding tables
    "margin"               : 1.0,
    "candidate_num"        : 200,  # Sampled negative candidates for evaluation
    "K"                    : 10,  # Metric of HR@K

    "gamma"                : 0.1,  # Multi-task learning rate between the two types of loss
    "epsilon"              : 1e-9,  # Small value for avoiding dividing zeros in distortion optimization (loss)

    "embedding_initializer": tf.initializers.glorot_uniform(),  # Initializer for the embedding tables
    "shuffle_buffer"       : 10000,  # Buffer size for TF's shuffling over the dataset
}

pos_set = None

def load_data(data):
    global user_count, item_count, pos_set
    user_count = len(data)+1
    item_count = max([max(_) for _ in data]) + 1
    train_data, test_data = [], []
    for uid, item_list in enumerate(data):
        for i in item_list[1:]:
            train_data.append((uid + 1, i))
        test_data.append((uid + 1, item_list[1]))
    record_count = len(train_data)
    print("[TRAINING] Loading data completed! User count: {}, item count: {}, training record count: {}".format(
        user_count, item_count, record_count))
    pos_set = set(train_data)
    train_data = np.array(train_data)
    return train_data, np.array(test_data), user_count, item_count

def construct_batch(ui_pair):
    while True:
        neg_i = rd.randint(1, item_count - 1)
        if (ui_pair[0], neg_i) not in pos_set:
            break
    return np.array((list(ui_pair) + [neg_i]), dtype=np.int32)


def construct_test(ui_pair):
    candidate_set = {ui_pair[1]}
    for _ in range(training_config["candidate_num"]):
        while True:
            neg_i = rd.randint(1, item_count - 1)
            if neg_i not in candidate_set and (ui_pair[0], neg_i) not in pos_set:
                candidate_set.add(neg_i)
                break
    candidate_set.remove(ui_pair[1])
    return np.array(list(ui_pair) + list(candidate_set), dtype=np.int32)


def get_train_batch(data):
    dataset = tf.data.Dataset.from_tensor_slices(data)
    dataset = dataset.map(lambda x: tf.py_func(construct_batch, [x], tf.int32))
    dataset = dataset.shuffle(buffer_size=training_config["shuffle_buffer"]).batch(training_config["batch_size"],
                                                                                   drop_remainder=False).repeat()
    data_iter = dataset.make_one_shot_iterator()
    batch = data_iter.get_next()
    return batch

def get_test_batch(data):
    dataset = tf.data.Dataset.from_tensor_slices(data)
    dataset = dataset.map(lambda x: tf.py_func(construct_test, [x], tf.int32))
    dataset = dataset.batch(training_config["batch_size"])
    data_iter = dataset.make_initializable_iterator()
    init = data_iter.initializer
    batch = data_iter.get_next()
        
    return batch, init


import tensorflow as tf
from curvlearn.manifolds import euclidean

def getCurvature(name, manifold, init_val=training_config["init_curvature"]):
    with tf.variable_scope("curvature", reuse=tf.AUTO_REUSE) as scope:
        return tf.get_variable(
            name=name,
            dtype=manifold.dtype,
            initializer=tf.constant(init_val, dtype=manifold.dtype),
            trainable=training_config["trainable_curvature"]
        )

class HyperMLModel(object):
    def __init__(self, user_count, item_count, bpr_pair, is_training=True):
        self._user_count = user_count
        self._item_count = item_count
        self._dim = training_config["dim"]
        self._manifold = training_config["manifold"]
        self._eclidean_manifold = euclidean.Euclidean()
        self._input_ids = bpr_pair
        self._is_training = is_training
        self._c = getCurvature("hyperml_curvature", self._manifold)
        with tf.variable_scope("hyperml", reuse=tf.AUTO_REUSE):
            self.embedding_table_user = tf.get_variable(name="RiemannianParameter/user_embedding", shape=(self._user_count, self._dim),
                                                        dtype=tf.float32,
                                                        initializer=training_config["embedding_initializer"])
            self.embedding_table_user1 = self._manifold.variable(self.embedding_table_user, self._c)
            self.embedding_table_item = tf.get_variable(name="RiemannianParameter/item_embedding", shape=(self._item_count, self._dim),
                                                        dtype=tf.float32,
                                                        initializer=training_config["embedding_initializer"])
            self.embedding_table_item1 = self._manifold.variable(self.embedding_table_item, self._c)
        if self._is_training:
            self.uid, self.pos_iid, self.neg_iid = tf.split(self._input_ids, 3, axis=1)
            #create 3 columns: uid, pos_iid e neg_iid of items
            self.user_vectors = tf.nn.embedding_lookup(self.embedding_table_user1, self.uid)
            #user vectors of some ids 
            self.pos_item_vectors = tf.nn.embedding_lookup(self.embedding_table_item1, self.pos_iid)
            #user vectors of some positive ids of items
            self.neg_item_vectors = tf.nn.embedding_lookup(self.embedding_table_item1, self.neg_iid)
            self._dist_pos = self._manifold.distance(self.user_vectors, self.pos_item_vectors, self._c)
            self._dist_neg = self._manifold.distance(self.user_vectors, self.neg_item_vectors, self._c)
            self.loss_p = tf.reduce_mean(tf.nn.relu(
                tf.reduce_sum(self._dist_pos, axis=-1) - tf.reduce_sum(self._dist_neg, axis=-1) +
                training_config["margin"]))
            d_Dij = self._dist_pos
            d_Eij = self._eclidean_manifold.distance(self._manifold.to_tangent(self.user_vectors, self._c),
                                                     self._manifold.to_tangent(self.pos_item_vectors, self._c), self._c)
            d_Dik = self._dist_neg
            d_Eik = self._eclidean_manifold.distance(self._manifold.to_tangent(self.user_vectors, self._c),
                                                     self._manifold.to_tangent(self.neg_item_vectors, self._c), self._c)
            self.loss_d1 = tf.reduce_mean(tf.nn.relu(tf.abs(d_Dij**0.5 - d_Eij**0.5) / (d_Eij**0.5 + training_config["epsilon"])))
            self.loss_d2 = tf.reduce_mean(tf.nn.relu(tf.abs(d_Dik**0.5 - d_Eik**0.5) / (d_Eik**0.5 + training_config["epsilon"])))
            self.loss = self.loss_p + training_config["gamma"] * (self.loss_d1 + self.loss_d2)
            self.optimizer = training_config["optimizer"](learning_rate=training_config["learning_rate"],
                                                          manifold=self._manifold, c=self._c)
            self.train_op = self.optimizer.minimize(self.loss)
        else:
            self.uid, self.can_iid = tf.split(self._input_ids, [1, training_config["candidate_num"] + 1], axis=1)
            #shape uid - [750,1]
            #shape can_idd - [750, 101]
            #split into one column of 1 - uid and 101 for the second column - can_iid
            self.user_vectors = tf.tile(tf.nn.embedding_lookup(self.embedding_table_user1, self.uid),
                                        [1, training_config["candidate_num"] + 1, 1])
            #from shape [750,200] vamos multiplicar por [1,101,1] levando a uma shape de [750,200,101]
            #creates a tensor by multiplication [1,101,1]
            self.can_item_vectors = tf.nn.embedding_lookup(self.embedding_table_item1, self.can_iid)
            #table with the coordinates of items per some specific indexes
            self.predict_scores = tf.squeeze(
                self._manifold.distance(self.user_vectors, self.can_item_vectors, self._c))
            self.rank_items = tf.argsort(self.predict_scores)[:, :training_config["K"]]
            self.correct = tf.get_variable(name="correct", shape=(), dtype=tf.int32,
                                           initializer=tf.zeros_initializer(dtype=tf.int32))
            self.total = tf.get_variable(name="total", shape=(), dtype=tf.int32,
                                         initializer=tf.zeros_initializer(dtype=tf.int32))
            self.metric_initializers = [self.correct.initializer, self.total.initializer]
            self.updata_correct = tf.assign_add(self.correct,
                                                tf.reduce_sum(tf.cast(tf.equal(self.rank_items, 0), tf.int32)))
            self.update_total = tf.assign_add(self.total, tf.shape(self.uid)[0])
            with tf.control_dependencies([self.updata_correct, self.update_total]):
                self.hr_at_k = tf.cast(self.correct, tf.float32) / tf.cast(self.total, tf.float32)
            
