import tensorflow as tf

from curvlearn.manifolds import PoincareBall
from curvlearn.optimizers import RAdam

training_config = {
    "manifold"             : PoincareBall(),  # The manifold to be used in the model
    "init_curvature"       : -1.0,  # Default value of the curvature of the manifold
    "trainable_curvature"  : True,  # Whether the curvatures are trainable

    "training_steps"       : 10000,  # Steps for training
    "log_steps"            : 100,  # Log intervals during training
    "eval_steps"           : 1000,  # Evaluation intervals during training

    "batch_size"           : 700,  # Batch size used in training
    "learning_rate"        : 0.01,  # Learning rate of training
    "optimizer"            : RAdam,  # The optimizer to be used in training

    "dim"                  : 200, # Dimension of the embedding tables
    "margin"               : 1.0,
    "candidate_num"        : 200,  # Sampled negative candidates for evaluation
    "K"                    : 10,  # Metric of HR@K

    "gamma"                : 0.1,  # Multi-task learning rate between the two types of loss
    "epsilon"              : 1e-9,  # Small value for avoiding dividing zeros in distortion optimization (loss)

    "embedding_initializer": tf.initializers.glorot_uniform(),  # Initializer for the embedding tables
    "shuffle_buffer"       : 10000,  # Buffer size for TF's shuffling over the dataset
}
