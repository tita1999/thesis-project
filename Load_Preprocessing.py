from gensim.models.poincare import PoincareModel, PoincareKeyedVectors
import pandas as pd


def load_poincare_gensim(input_filename):
    """Load embedding trained via Gensim PoincareModel.

    Parameters
    ----------
    filepath : str
        Path to model file.

    Returns:
        PoincareKeyedVectors instance.

    """
    model = PoincareModel.load(input_filename)
    return model.kv


#Loading Facebook categories

def load_fb_data(data):
    dictn = {"Tag EN - 2" : "Tag EN - 1",
                "Tag EN - 3" : "Tag EN - 2"}
    
    fb_cat = pd.read_excel(data, header=1)
    fb_cat = fb_cat[["Tag EN "]][:1568]
    
    # split into 3 columns
    fb_cat[['Tag EN - 1', 'Tag EN - 2', "Tag EN - 3"]] = fb_cat['Tag EN '].str.split('|', 2, expand=True)
    fb_cat = fb_cat.applymap(lambda x: x.strip() if isinstance(x, str) else x)
    master_ = fb_cat["Tag EN - 1"].drop_duplicates().values
    master = pd.DataFrame(list(zip(master_, master_)), columns = ["Tag EN - 1", "Tag EN - 2"])
    fb_cat.dropna(subset=['Tag EN - 2'], how='all', inplace=True)
    fb_cat.drop("Tag EN ", axis=1, inplace=True)
    
    data_3d = fb_cat[fb_cat["Tag EN - 3"].isna() == False]
    data_new = data_3d[["Tag EN - 1","Tag EN - 3"]]
    data_new.rename(columns=dictn, inplace=True)
    d_23 = data_3d[["Tag EN - 2","Tag EN - 3"]] #data with only 2 columns with no nan's
    d_23.rename(columns=dictn, inplace=True) #rename the columns to be 1,2
    fb_cat.drop("Tag EN - 3", axis=1, inplace=True) #drop column 3
    
    # Create the final table
    new_data = fb_cat.append(d_23)
    new_data = new_data.append(master)
    new_data = new_data.append(data_new)
    new_data = new_data[["Tag EN - 2", "Tag EN - 1"]]
    new_data = new_data.dropna(how="all", axis=0)
    new_data = new_data.drop_duplicates()
    
    tuples = [tuple(x) for x in new_data.values]

    return tuples

def users_categories_positive_data(data_path, i=1):
     
    try:
        try:
            prof_data = pd.read_csv(data_path, encoding='latin1') 
        except:
            prof_data= data_path
        insights_data = prof_data[(prof_data["type"] == "INSIGHT") & ((prof_data["value"] == "TRUE") | (prof_data["value"] == 1))]
        print("The user has %d interests." %len(insights_data))

        string = "Has interest in"
        categories = []
        for interest in insights_data["profileID"]:
            if string in interest:
                new = interest.replace(string, "")
                categories.append(new)
            else:
                categories.append(interest)
    
    except:
        try:
            df = pd.read_csv(data_path)
        except:
            df = data_path
        user_cat = df.iloc[[i]][df.iloc[[i]]==1]
        categories = list(user_cat.dropna(axis = "columns", how="all"))
        
    data = pd.DataFrame(categories, columns = ["Tag EN"])
    data[['Tag EN - 1', 'Tag EN - 2', "Tag EN - 3"]] = data['Tag EN'].str.split('|', 2, expand=True)
    data = data.applymap(lambda x: x.strip() if isinstance(x, str) else x)
    concat = pd.concat([data["Tag EN - 1"], data["Tag EN - 2"], data["Tag EN - 3"]])
    pos_cats = list(set(concat))
    pos_cats = pd.Series(pos_cats)
    pos_cats = pos_cats.dropna(axis=0, how="all")
    
    return pos_cats


def users_categories_negative_data(data_path, i=1):
    
    try:
        try:
            prof_data = pd.read_csv(data_path, encoding='latin1') 
        except:
            prof_data = data_path
        non_insights_data = prof_data[(prof_data["type"] == "INSIGHT") & (prof_data["value"] == "FALSE") | (prof_data["value"] == -1)]
        print("The user has %d explicit negative interests." %len(non_insights_data))

        string = "Has interest in"
        negative_categories = []
        for interest in non_insights_data["profileID"]:
            if string in interest:
                new = interest.replace(string, "")
                negative_categories.append(new)
            else:
                categories.append(interest)           
    except:
        try:
            df = pd.read_csv(data_path)
        except:
            df = data_path
        user_cat = df.iloc[[i]][df.iloc[[i]]==-1]
        negative_categories = list(user_cat.dropna(axis = "columns", how="all"))
                
    data = pd.DataFrame(negative_categories, columns = ["Tag EN"])
    try:
        data[['Tag EN - 1', 'Tag EN - 2', "Tag EN - 3"]] = data['Tag EN'].str.split('|', 2, expand=True)
        data = data.applymap(lambda x: x.strip() if isinstance(x, str) else x)
        data.drop(["Tag EN"], inplace=True, axis=1)
        concat = pd.concat([data["Tag EN - 1"], data["Tag EN - 2"], data["Tag EN - 3"]])
        
    except:
        data[['Tag EN - 1', 'Tag EN - 2']] = data['Tag EN'].str.split('|', 1, expand=True)
        data = data.applymap(lambda x: x.strip() if isinstance(x, str) else x)
        data.drop(["Tag EN"], inplace=True, axis=1)
        concat = pd.concat([data["Tag EN - 1"], data["Tag EN - 2"]])
    
    neg_cats = list(set(concat))
    neg_cats = pd.Series(neg_cats)
    neg_cats = neg_cats.dropna(axis=0, how="all")
    
    return neg_cats